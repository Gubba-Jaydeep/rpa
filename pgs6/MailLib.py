import imaplib
import base64
import os
import email
class MailLib:

    def __init__(self):
        self.mail = imaplib.IMAP4_SSL('imap.gmail.com', 993)
        self.email=""
        self.password=""
        self.data=[]


    def open_mail(self,email,password):
        self.email=str(email)
        self.password=str(password)
        self.mail.login(self.email, self.password)

    def select_label(self):

        self.mail.select('Inbox')

    def search(self,subject):
        t, self.data = self.mail.search(None,'SUBJECT', subject)

    def download_Files(self,dpath):
        mail_ids = self.data[0]
        id_list = mail_ids.split()
        for num in id_list:
            typ, data = self.mail.fetch(num, '(RFC822)')
            raw_email = data[0][1]
            # converts byte literal to string removing b''
            raw_email_string = raw_email.decode('utf-8')
            email_message = email.message_from_string(raw_email_string)
            # downloading attachments
            for part in email_message.walk():
                # this part comes from the snipped I don't understand yet...
                if part.get_content_maintype() == 'multipart':
                    continue
                if part.get('Content-Disposition') is None:
                    continue
                fileName = part.get_filename()
                if bool(fileName):
                    filePath = os.path.join(dpath, fileName)
                    if not os.path.isfile(filePath):
                        fp = open(filePath, 'wb')
                        fp.write(part.get_payload(decode=True))
                        fp.close()




