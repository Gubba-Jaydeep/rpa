*** Settings ***
Documentation     For Finance
Resource          resource.robot


Suite Setup     Initialize Selenium Params




Suite Teardown        Close Browser

*** Keywords ***

Entire Process
    Upload Login

    Empty Directory         ${download_path}
    Download PayU
    Upload PayU

    Empty Directory         ${download_path}
    Download HDFC 027 and 8343
    Upload HDFC 027 and 8343

    Empty Directory         ${download_path}
    Download HDFC Wallet
    Upload HDFC Wallet

    Empty Directory         ${download_path}
    Download CC New
    Upload CC New

    Empty Directory         ${download_path}
    Download CC Avenue
    Upload CC Avenue


*** Test Cases ***
TestCAses
    Initialize Suite Variables
    Initialize Browser

    ${fail}=  Set Variable  0
    :FOR  ${index}  IN RANGE  50
    \  ${passed}=       Run Keyword and Return Status        Entire Process
    \  Continue For Loop If  ${passed}
    \  ${fail}=  ${fail} + 1
    ${success}=  Set Variable  5 - ${fail}
    Log Many   Success:  ${success}
    Log Many   fail:  ${fail}

    # Download HDFC 027 and 8343       10-Jun-2019
    # Upload HDFC 027 and 8343         10-Jun-2019





