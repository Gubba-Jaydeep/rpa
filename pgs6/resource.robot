*** Settings ***
Documentation     Finance
Library           SeleniumLibrary
Library			  Collections
Library			  ExcelLibrary
Library           ImapLibrary
#Library			  MailLib.py
Library           OperatingSystem
Library           String
Library			  BuiltIn
Library			  DateTime
Library			  Process
Library  		  DatabaseLibrary
Library           MailLib.py



*** Variables ***

${DBHost}         localhost
${DBName}         rpa
${DBPass}         password
${DBPort}         3306
${DBUser}         rpa
${DBTable}        finance

${Userdefined_date}             27-May-2019
${Userdefined_date2}             27/05/2019





*** Keywords ***



Initialize Suite Variables
	Connect To Database       pymysql      ${DBName}    ${DBUser}    ${DBPass}    ${DBHost}    ${DBPort}

	@{Browser1}=    Query     select name from ${DBTable} where identifier='Browser';
	@{Download_URL1}=    Query     select name from ${DBTable} where identifier='Download_URL';
  	@{Upload_URL1}=    Query     select name from ${DBTable} where identifier='Upload_URL';
  	@{gmail_id1}=    Query     select name from ${DBTable} where identifier='gmail_id';
  	@{gmail_password1}=    Query     select name from ${DBTable} where identifier='gmail_password';
  	@{gmail_URL1}=    Query     select name from ${DBTable} where identifier='gmail_url';
  	@{DELAY1}=    Query     select name from ${DBTable} where identifier='DELAY';
	@{file_path1}=    Query     select name from ${DBTable} where identifier='file_path';
	@{download_path1}=    Query     select name from ${DBTable} where identifier='download_path';
	@{upload_id_pass1}=    Query     select name from ${DBTable} where identifier='upload_id_pass';

	Set Global Variable				${Browser}					${Browser1[0][0]}
	Set Global Variable				${Download_URL}				${Download_URL1[0][0]}
	Set Global Variable				${Upload_URL}				${Upload_URL1[0][0]}
	Set Global Variable				${gmail_id}					${gmail_id1[0][0]}
	Set Global Variable				${gmail_password}			${gmail_password1[0][0]}
	Set Global Variable				${gmail_URL}				${gmail_URL1[0][0]}
	Set Global Variable				${DELAY}					${DELAY1[0][0]}
	Set Global Variable				${file_path}				${file_path1[0][0]}
	Set Global Variable				${download_path}			${download_path1[0][0]}
	Set Global Variable				${upload_id_pass}			${upload_id_pass1[0][0]}


	${Yeterday_Date} =		Get Current Date    result_format=%d-%b-%Y    increment=-1 day
	${Yeterday_Date2} =		Get Current Date    result_format=%d-%m-%Y    increment=-1 day
	${MIS_Files}	Create List		AIR_DOM_BOOK		AIR_INT_BOOK		HOTEL_MIS_BOOK		pay_failure_mis			TRAIN_BOOK
	${BOS_Files}	Create List		India_activity_sale_bos		India_air_amend_bos		India_air_amend_rebook_bos		India_air_sale_bos		India_hotel_sale_bos		train_sale_bos		train_refund_bos
	Set Global Variable				${MIS_Files}
	Set Global Variable				${Yeterday_Date}
	Set Global Variable				${Yeterday_Date2}
	Set Global Variable				${BOS_Files}


Initialize Selenium Params
	# Set Selenium Speed   0.3
	Set Selenium Timeout    1000
	Set Selenium Implicit Wait    60


Initialize Browser
# http://gubba.jaydeep@cleartrip.com:Password254@c@summary.cleartrip.com/mis_reports/27-May-2019
	Open Browser		https://info.payu.in/merchant/salt		Chrome
	Maximize Browser Window


Upload Login
	Go To 		${Upload_URL}/
	Input Text 		id:Username			${upload_id_pass}
	Input Text 		id:Password			${upload_id_pass}
	Click Element		id:btnlogin



Upload with locator
	[Arguments]			${fileName}		${type}		${checkbox_locator}
	#${checkbox_locator}=		Convert To Integer		${checkbox_locator}
	#Log			(//input[@class='chk${type}'])[${checkbox_locator}]
	Wait Until Element Is Visible		(//input[@class='chk${type}'])[${checkbox_locator}]
	Click Element			(//input[@class='chk${type}'])[${checkbox_locator}]
	Choose File			id=fileupload${type}				${fileName}
	# Click Element		btnSubmit${type}
	Sleep		10s
	Wait Until Element Is Visible		(//input[@class='chk${type}'])[${checkbox_locator}]



Download PayU
	[Arguments]         ${date}=${EMPTY}
    ${date}=        Set Variable If             "${date}"=="${EMPTY}"            ${Yeterday_Date}           ${date}

	Go To 			https://info.payu.in/merchant/salt
    Wait Until Element is Visible       xpath://*[@id="UserName"]
    Input Text  xpath://*[@id="UserName"]     treasury
    Wait Until Element is Visible       xpath://*[@id="Alias"]
    Input Text  xpath://*[@id="Alias"]     cleartrip
    Wait Until Element is Visible     xpath://*[@id="Password"]
    Input Text  xpath://*[@id="Password"]     cleartrip1
    Wait Until Element is Visible         //*[@id="SignInForm"]/div/div[2]/div[2]/div[4]/button/strong
    Click Element               //*[@id="SignInForm"]/div/div[2]/div[2]/div[4]/button/strong
    Wait Until Element is Visible       //*[@id="SideBar"]/div[1]/ul/li[4]/a
    Click Element         //*[@id="SideBar"]/div[1]/ul/li[4]/a
    Wait Until Element is Visible         //*[@id="Completed"]/div[1]/table/tbody/tr[2]/td[2]/div/strong/span
    Click Element         //*[@id="Completed"]/div[1]/table/tbody/tr[2]/td[2]/div/strong/span
    Wait Until Element is Visible       //*[@id="utrDetails"]/div[2]/div[3]/a
    Click Element         //*[@id="utrDetails"]/div[2]/div[3]/a
    Wait Until Element is Visible         //*[@id="SearchBarDiv"]/div[1]/div[2]/form/input
    ${PayU}=       Get Value          //*[@id="SearchBarDiv"]/div[1]/div[2]/form/input
    Set Global Variable				${PayU}


	Sleep 			3s
    Wait Until Element is Visible			//*[@id="TxnAllData"]/div[1]/div/div[2]/a[1]
	Click Element 			//*[@id="TxnAllData"]/div[1]/div/div[2]/a[1]
	Sleep  		10s
	Input Text			//*[@id="ng-app"]/body/div[9]/div[2]/div[2]/div[1]/form/div[2]/div/input 			${PayU}
	Click Element 			//*[@id="ng-app"]/body/div[9]/div[2]/div[2]/div[1]/form/div[3]/button

# Wait For Download
	Go To 		https://info.payu.in/merchant/downloads
	Sleep 			8 minutes
	Go To 		https://info.payu.in/merchant/downloads
	Click Element			//*[@id="Page"]/div[3]/div/table/tbody/tr[1]/td[3]/p
	Sleep		5s
# Unzip and Move PayU
	# ${PayU}=		Set Variable		HDFCR52019061081751017
	${path}=		Normalize Path		${download_path}
	Run Process			 unzip ${PayU}_*.zip		 shell=TRUE 		cwd=${path}
	Sleep 		10s
	${req_file}=		List Files In Directory			${download_path}		${PayU}_*.xls			absolute
	Log			${req_file}
	Run Process			 ssconvert ${PayU}_*.xls @{req_file}[0]x		 shell=TRUE 		cwd=${path}
	Move File		@{req_file}[0]x			${file_path}/${date}/PayU/




Upload PayU
	[Arguments]         ${date}=${EMPTY}
    ${date}=        Set Variable If             "${date}"=="${EMPTY}"            ${Yeterday_Date}           ${date}
	Go To 	   			${Upload_URL}/Upload/UploadFile
	${req_file}=		List Files In Directory			${file_path}/${date}/PayU		${PayU}_*.xlsx		absolute
	Upload with locator			@{req_file}[0]		\			52




Download HDFC 027 and 8343
    [Arguments]         ${date}=${EMPTY}
    ${date}=        Set Variable If             "${date}"=="${EMPTY}"            ${Yeterday_Date}           ${date}


    Remove Files			${download_path}/*.zip
    Remove Files			${download_path}/*.xls
    Remove Files			${download_path}/*.XLS
    Remove Files			${download_path}/*.xlsx

    Open Mail       rpa.finance@cleartrip.com           Rcleartrip@12
    Select Label
    Search          "Email MPR as of ${date}"
    ${path}=			Normalize Path		${download_path}

    download Files 			${path}/
	Sleep		10s

	Run Process			 unzip -P C08832 99010027-*.zip		 shell=TRUE 		cwd=${path}
	# Sleep 		10s
	Run Process			 unzip -P J90315 99018343-*.zip		 shell=TRUE 		cwd=${path}
	# Sleep 			5s


	${req_file}=		List Files In Directory			${download_path}		99010027-*.XLS			absolute
	${f_name}=		Fetch From Left 		@{req_file}[0]			.XLS
	Run Process			 ssconvert 99010027-*.XLS ${f_name}.xlsx		 shell=TRUE 		cwd=${path}
	Move File		${f_name}.xlsx			${file_path}/${date}/HDFC/

	${req_file}=		List Files In Directory			${download_path}		99018343-*.XLS			absolute
	${f_name}=		Fetch From Left 		@{req_file}[0]			.XLS
	Run Process			 ssconvert 99018343-*.XLS ${f_name}.xlsx		 shell=TRUE 		cwd=${path}
	Move File		${f_name}.xlsx			${file_path}/${date}/HDFC/


Upload HDFC 027 and 8343
	[Arguments]         ${date}=${EMPTY}
    ${date}=        Set Variable If             "${date}"=="${EMPTY}"            ${Yeterday_Date}           ${date}

	Go To 	   			${Upload_URL}/Upload/UploadFile
	${req_file}=		List Files In Directory			${file_path}/${date}/HDFC		99010027-*.xlsx		absolute
	Upload with locator			@{req_file}[0]		\			25

	${req_file}=		List Files In Directory			${file_path}/${date}/HDFC		99018343-*.xlsx		absolute
	Upload with locator			@{req_file}[0]		\			29


Download HDFC Wallet
	[Arguments]         ${date}=${EMPTY}
    ${date}=        Set Variable If             "${date}"=="${EMPTY}"            ${Yeterday_Date}           ${date}

	Go To 		https://pg-csr.wibmo.com/pgcsr/merchant/index.jsp

	Wait Until Element is Visible   xpath:/html/body/table/tbody/tr[2]/td/table/tbody/tr[3]/td[2]/input
    Input Text        xpath:/html/body/table/tbody/tr[2]/td/table/tbody/tr[3]/td[2]/input         Treasury
    Wait Until Element is Visible             xpath:/html/body/table/tbody/tr[2]/td/table/tbody/tr[4]/td[2]/input
    Input Text        xpath:/html/body/table/tbody/tr[2]/td/table/tbody/tr[4]/td[2]/input         matrix139
    Wait Until Element is Visible         xpath:/html/body/table/tbody/tr[2]/td/table/tbody/tr[5]/td[2]/input
    Input Text        xpath:/html/body/table/tbody/tr[2]/td/table/tbody/tr[5]/td[2]/input           18699706
    Wait Until Element is Visible             xpath:/html/body/table/tbody/tr[2]/td/table/tbody/tr[6]/td[2]/input
    Input Text            xpath:/html/body/table/tbody/tr[2]/td/table/tbody/tr[6]/td[2]/input         hdfcpg
    Wait Until Element is Visible         xpath:/html/body/table/tbody/tr[2]/td/table/tbody/tr[7]/td/center/input
    Click Element             xpath:/html/body/table/tbody/tr[2]/td/table/tbody/tr[7]/td/center/input
    Wait Until Element is Visible         xpath:/html/body/table/tbody/tr[2]/td[1]/table/tbody/tr[14]/td[2]/a
    Click Element           xpath:/html/body/table/tbody/tr[2]/td[1]/table/tbody/tr[14]/td[2]/a
    Wait Until Element is Visible         xpath:/html/body/table/tbody/tr[2]/td[2]/table[1]/tbody/tr[1]/td/form/table[2]/tbody/tr[4]/td[1]/center/a/img
    Click Element           xpath:/html/body/table/tbody/tr[2]/td[2]/table[1]/tbody/tr[1]/td/form/table[2]/tbody/tr[4]/td[1]/center/a/img

	Sleep		10s
	${path}=			Normalize Path		${download_path}
    ${req_file}=		List Files In Directory			${download_path}		CLEARTRIP-*.csv			absolute
    ${f_name}=		Fetch From Left 		@{req_file}[0]			.csv
    Run Process			ssconvert CLEARTRIP-*.csv ${f_name}.xlsx		 shell=TRUE 		cwd=${path}
	Sleep		5s
    Move File		${f_name}.xlsx			${file_path}/${date}/HDFC_Wallet/

Upload HDFC Wallet
	[Arguments]         ${date}=${EMPTY}
    ${date}=        Set Variable If             "${date}"=="${EMPTY}"            ${Yeterday_Date}           ${date}

	Go To 	   			${Upload_URL}/Upload/UploadFile
	${req_file}=		List Files In Directory			${file_path}/${date}/HDFC_Wallet		CLEARTRIP-*.xlsx		absolute
	Upload with locator			@{req_file}[0]		\			28



Download CC New
	[Arguments]         ${date}=${EMPTY}
    ${date}=        Set Variable If             "${date}"=="${EMPTY}"            ${Yeterday_Date}           ${date}

	#Using MailLib
	# Open Mail       rpa.finance@cleartrip.com           Rcleartrip@12
    Select Label
    Search          "CCAvenue Orders Settlement Report- ${Yeterday_Date2}"
    ${path}=			Normalize Path		${download_path}

    download Files 			${path}/
	Sleep		10s

	Run Process			 unzip SettlementReport_*.zip		 shell=TRUE 		cwd=${path}
	Sleep 		10s
	${req_file}=		List Files In Directory			${download_path}		SettlementReport_*.xls			absolute
	Log			${req_file}
	${f_name}=		Fetch From Left 		@{req_file}[0]			(
	Run Process			 ssconvert SettlementReport_*.xls ${f_name}.xlsx		 shell=TRUE 		cwd=${path}
	Move File		${f_name}.xlsx			${file_path}/${date}/CC_New/


Upload CC New
	[Arguments]         ${date}=${EMPTY}
    ${date}=        Set Variable If             "${date}"=="${EMPTY}"            ${Yeterday_Date}           ${date}

	Go To 	   			${Upload_URL}/Upload/UploadFile
	${req_file}=		List Files In Directory			${file_path}/${date}/CC_New		SettlementReport_*.xlsx		absolute
	Upload with locator			@{req_file}[0]		\			16


Download CC Avenue
	[Arguments]         ${date}=${EMPTY}
    ${date}=        Set Variable If             "${date}"=="${EMPTY}"            ${Yeterday_Date}           ${date}

	Go To 		https://mars.ccavenue.com/mer_register/memberLogin_ccav.jsp

    Wait Until Element is Visible         //*[@id="maincontenttable"]/tbody/tr[2]/td/form/table/tbody/tr[1]/td/table/tbody/tr/td/table/tbody/tr[1]/td[2]/input
    Input Text          //*[@id="maincontenttable"]/tbody/tr[2]/td/form/table/tbody/tr[1]/td/table/tbody/tr/td/table/tbody/tr[1]/td[2]/input              kar18423
    Wait Until Element is Visible         //*[@id="maincontenttable"]/tbody/tr[2]/td/form/table/tbody/tr[1]/td/table/tbody/tr/td/table/tbody/tr[2]/td[2]/input
    Input Text          //*[@id="maincontenttable"]/tbody/tr[2]/td/form/table/tbody/tr[1]/td/table/tbody/tr/td/table/tbody/tr[2]/td[2]/input          Cleartrip@2
    Wait Until Element is Visible         //*[@id="maincontenttable"]/tbody/tr[2]/td/form/table/tbody/tr[3]/td/input
    Click Element           //*[@id="maincontenttable"]/tbody/tr[2]/td/form/table/tbody/tr[3]/td/input
    Wait Until Element is Visible       //*[@id="/mer_register/WeekStatement.jsp"]
    Click Element           //*[@id="/mer_register/WeekStatement.jsp"]
    Select Checkbox       name:pay1
    # Click Element       //*[@id="form1"]/table/tbody/tr[2]/td/table/tbody/tr[3]/td[4]/font/input
    Wait Until Element is Visible       //*[@id="form1"]/center/input[2]
    Click Element         //*[@id="form1"]/center/input[2]
    Wait Until Element is Visible       xpath:/html/body/table[3]/tbody/tr/td[4]/table[3]/tbody/tr/td[2]/table/tbody/tr/td/table/tbody/tr[10]/td/div/font[2]/b/a
    Click Element         xpath:/html/body/table[3]/tbody/tr/td[4]/table[3]/tbody/tr/td[2]/table/tbody/tr/td/table/tbody/tr[10]/td/div/font[2]/b/a
    Wait Until Element is Visible         xpath:/html/body/table[4]/tbody/tr/td/table/tbody/tr[1]/td/table/tbody/tr/td[3]/a[1]/img
    Click Element           xpath:/html/body/table[4]/tbody/tr/td/table/tbody/tr[1]/td/table/tbody/tr/td[3]/a[1]/img
    Sleep         10s
    ${path}=		Normalize Path		${download_path}
	Run Process			 unzip Account_Summary_Report*.zip		 shell=TRUE 		cwd=${path}
	Sleep 		10s
	${req_file}=		List Files In Directory			${download_path}		Account_Summary_Report*.xls			absolute
	Log			${req_file}
	Run Process			 ssconvert Account_Summary_Report*.xls @{req_file}[0]x		 shell=TRUE 		cwd=${path}
	Move File		@{req_file}[0]x			${file_path}/${date}/CC_AVENUE/


    # Extract Zip File     /home/nikhilgupta/Downloads/Account_Summary_Report.zip

Upload CC Avenue
	[Arguments]         ${date}=${EMPTY}
    ${date}=        Set Variable If             "${date}"=="${EMPTY}"            ${Yeterday_Date}           ${date}

	Go To 	   			${Upload_URL}/Upload/UploadFile
	${req_file}=		List Files In Directory			${file_path}/${date}/CC_AVENUE		Account_Summary_Report*.xlsx		absolute
	Upload with locator			@{req_file}[0]		\			14



