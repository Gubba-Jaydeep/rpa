*** Settings ***
Documentation     Finance
Library           SeleniumLibrary
Library			  Collections
Library			  ExcelLibrary
Library           ImapLibrary
Library           OperatingSystem
Library           String
Library			  BuiltIn
Library			  DateTime
Library			  Process
Library  		  DatabaseLibrary


*** Variables ***
${Main_URL}         http://rpa.finance@cleartrip.com:Rcleartrip@12@helpdesk.cleartrip.com/services/indexnew.html
${DELAY}			0.3

*** Keywords ***



Initialize Suite Variables
    ${Spicejet}     Create List		BOMCL23479      BOMCL97423      CORPORATE / BOMCS40554      BOMCLA9742      			BOMCLE9742      BOMTATA003      BOMCLB9742      SGCPN_W001      NONPYTM002      BOMCLC9742      SMEMTR0001	
	${Indigo}		Create List		OTI003			OTB003			LCT001						CORPORATE / KTBOM089		COUPON / 1430772			OTB004			MBOTI003		STACT001		OTI029H
    ${GoAir}		Create List		RXAPICLR


	
	Set Global Variable         ${Spicejet}
	Set Global Variable         ${Indigo}
	Set Global Variable         ${GoAir}

Initialize Selenium Params
	Set Selenium Speed   ${DELAY}
	Set Selenium Timeout    60s
	Set Selenium Implicit Wait    60


Initialize Browser
	Open Browser		${Main_URL}		Chrome
	Maximize Browser Window

Insert Amount To Cell
    [Arguments]     ${row}      ${col}      ${amount}

	Open Excel Document		Phase1.xlsx  	doc1
	Write Excel Cell	${row}	${col}	${amount}			Sheet1
	Save Excel Document		Phase1.xlsx
	Close All Excel Documents

Get Spicejet Data
    [Arguments]         ${start}
    FOR     ${i}        IN      @{Spicejet}
        #Initialize Browser
		# Go To		http://www.google.com
		# Sleep		3s
		Go To		${Main_URL}
        Wait Until Element Is Visible       xpath://input[@value='${i}'][1]/..
        Click Element       xpath://input[@value='${i}'][1]/..
        
		
        Select Window           NEW
		#Wait Until Element Is Visible   //span[@class='agent-balance-amount']
        Sleep		5s
		${text}=        Get Text        //span[@class='agent-balance-amount']
        Insert Amount To Cell 	 	${start}  		3  		${text}
        ${start}=       Evaluate		${start} + 1

		#Click Link		//li[@style="z-index: 96;"]/a[@id="Login"]
		Sleep	2s
		Close Window
		Select Window			MAIN

		#repeat
		Go To		${Main_URL}
        Wait Until Element Is Visible       xpath://input[@value='${i}'][1]/..
        Click Element       xpath://input[@value='${i}'][1]/..
		Sleep	2s
		Close Window
		Select Window			MAIN
		#Close Browser
		
       
    END


Get Indigo Data
    [Arguments]         ${start}
    FOR     ${i}        IN      @{Indigo}
        #Initialize Browser
		# Go To		http://www.google.com
		# Sleep		3s
		Go To		${Main_URL}
        Wait Until Element Is Visible       xpath://input[@value='${i}'][1]/..
        Click Element       xpath://input[@value='${i}'][1]/..
        
		
        Select Window           NEW
		Sleep		5s
		#Wait Until Element Is Visible   //*[@id="AgentMAcc"]/div/div[2]/div/ul/li[3]/input
		${isKTBOM089}=	Run Keyword And Return Status	Should be equal as strings    ${i}    	CORPORATE / KTBOM089
		
		${text}=		Run Keyword If		${isKTBOM089}			Get Value       //*[@id="corpMAcc"]/div/div[2]/div/ul/li[3]/input
		...				ELSE		Get Value        //*[@id="AgentMAcc"]/div/div[2]/div/ul/li[3]/input
		Log			${text}
		#...				ELSE			Get Value        //*[@id="corpMAcc"]/div/div[2]/div/ul/li[3]/input
        # ${text}=        Get Value        //*[@id="AgentMAcc"]/div/div[2]/div/ul/li[3]/input
        Insert Amount To Cell 	 	${start}  		3  		${text}
        ${start}=       Evaluate		${start} + 1
		Run Keyword If		${isKTBOM089}			Click Element		//*[@id="CorptLogout"]/a
		...					ELSE					Click Element		//*[@id="AgentLogout"]/a

		
		
		Sleep	2s
		Close Window
		Select Window			MAIN


		#repeat
		
		#Close Browser
		
       
    END



Get GoAir Data
	[Arguments]         ${start}
    FOR     ${i}        IN      @{GoAir}
        #Initialize Browser
		# Go To		http://www.google.com
		# Sleep		3s
		Go To		${Main_URL}
        Wait Until Element Is Visible       xpath://input[@value='${i}'][1]/..
        Click Element       xpath://input[@value='${i}'][1]/..
        
		
        Select Window           NEW
		# //*[@id="moreAccountDetailsDiv"]/div[7]/strong
		# Wait Until Element Is Visible   //*[@id="agent-info-container"]/div[2]/p[2]/strong
		# Sleep		5s
		${text}=			Get Text       //*[@id="agent-info-container"]/div[2]/p[2]/strong
		Log			${text}
		#...				ELSE			Get Value        //*[@id="corpMAcc"]/div/div[2]/div/ul/li[3]/input
        # ${text}=        Get Value        //*[@id="AgentMAcc"]/div/div[2]/div/ul/li[3]/input
        Insert Amount To Cell 	 	${start}  		3  		${text}
        ${start}=       Evaluate		${start} + 1

		
		
		Sleep	2s
		Close Window
		Select Window			MAIN


		#repeat
		
		#Close Browser
		
       
    END

Get AirIndiaExpress Data
	[Arguments]         ${start}
	
	Go To		${Main_URL}
    Wait Until Element Is Visible       xpath://input[@value='AIR INDIA EXPRESS'][1]/..
    Click Element       xpath://input[@value='AIR INDIA EXPRESS'][1]/..
        
    Select Window           NEW
	Sleep		5s
	Wait Until Element Is Visible   //*[@id="ctlImageMenu_dltImageMenu_ctl00_hypCreditLimitPopup"]
	Click Element			//*[@id="ctlImageMenu_dltImageMenu_ctl00_hypCreditLimitPopup"]
	# Close Window
	Select Window		NEW
	Sleep		 10s

	${text}=			Get Text       //*[@id="tblCreditLimitInformation"]/tbody/tr[2]/td[2]
	Log			${text}
    Insert Amount To Cell 	 	${start}  		3  		${text}
    ${start}=       Evaluate		${start} + 1
	Sleep	2s
	Close Window
	Select Window			MAIN


Get Trujet Data
	[Arguments]         ${start}
	
	Go To		${Main_URL}
    Wait Until Element Is Visible       xpath://input[@value='TRUJET'][1]/..
    Click Element       xpath://input[@value='TRUJET'][1]/..
    Select Window           NEW

	Sleep		5s
	Wait Until Element Is Visible   //*[@id="header"]/div[1]/div/div/div[2]/div/div[2]/div[1]/strong
	${text}=			Get Text       //*[@id="header"]/div[1]/div/div/div[2]/div/div[2]/div[1]/strong
	Log			${text}
    Insert Amount To Cell 	 	${start}  		3  		${text}
    ${start}=       Evaluate		${start} + 1
	Sleep	2s
	Close Window
	Select Window			MAIN



Get AirAsia Data
	[Arguments]         ${start}
	
	Go To		${Main_URL}
    Wait Until Element Is Visible       xpath://input[@value='AIR ASIA'][1]/..
    Click Element       xpath://input[@value='AIR ASIA'][1]/..
    Select Window           NEW
	Sleep		5s
	#Click Element		//*[@id="UpdateProfileAgency"]/div/img
	Go To		https://booking2.airasia.com/UpdateProfileAgency.aspx
	Wait Until Element Is Visible   //*[@id="agencyForm"]/div[1]/p[5]
	${text}=			Get Text       //*[@id="agencyForm"]/div[1]/p[5]
	Log			${text}
    Insert Amount To Cell 	 	${start}  		3  		${text}
    ${start}=       Evaluate		${start} + 1
	Sleep	2s
	Close Window
	Select Window			MAIN

