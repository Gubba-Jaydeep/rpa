*** Settings ***
Documentation     Finance
Library           SeleniumLibrary
Library			  Collections
Library			  ExcelLibrary
Library           ImapLibrary
Library           OperatingSystem
Library           String
Library			  BuiltIn
Library			  DateTime
Library			  Process
Library  		  DatabaseLibrary


*** Variables ***
${Main_URL}         	http://rpa.finance@cleartrip.com:Rcleartrip@12@helpdesk.cleartrip.com/services/indexnew.html
${DELAY}				0.3
${download_path}		~/Downloads

*** Keywords ***



Initialize Suite Variables
    ${GoAir}		Create List		RXAPICLR
	${indigo_id}				Create List			oti003			otb003			lct001			ktbom089		1430772				OTB004				MBOTI003
	${indigo_password}			Create List			Clear$999		Indigo$123		Indigo$123		Indigo$123		Indigo$123			Indigo$123			bdpsppsa$3312


	Set Global Variable         ${GoAir}
	Set Global Variable         ${indigo_id}
	Set Global Variable         ${indigo_password}

Initialize Selenium Params
	Set Selenium Speed   ${DELAY}
	Set Selenium Timeout    60s
	Set Selenium Implicit Wait    60


Initialize Browser
	Open Browser		${Main_URL}		Chrome
	Maximize Browser Window


Select Date  [Arguments]  ${year}  ${month}  ${date}            
   ### This keyword evaluates the difference from current date and moves ahead/backward to select the month ###

   ${currentDate} =     Get Current Date    result_format=datetime
   Convert To Integer  ${year}
   Convert To Integer  ${month}
   Convert To Integer  ${date}
   ${month-diff}=  Evaluate  ${month}-${currentDate.month}
   ${year-diff}=  Evaluate  ${year}-${currentDate.year}
   ${move}=  Evaluate  ${year-diff}*12+${month-diff}

   ${shiftForward}=  Set Variable If  
   ...   ${move}>0  1
   ...   ${move}<0  0

   ${move}=  Set Variable If  
   ...   ${move}>0  ${move}
   ...   ${move}<0  ${move}*-1

   :FOR     ${var}  IN RANGE    ${move}
       \  Run Keyword If  ${shiftForward}==0  Click Element  css=svg path[d="M15.41 7.41L14 6l-6 6 6 6 1.41-1.41L10.83 12z"]
       \  Run Keyword If  ${shiftForward}==1  Click Element  css=svg path[d="M10 6L8.59 7.41 13.17 12l-4.58 4.59L10 18l6-6z"]

   Wait Until Element Is Enabled  xpath=//span[text()="${date}"]
   Click Element  xpath=//span[text()="${date}"]

Download GoAir Report


	
    FOR     ${i}        IN      @{GoAir}
        #Initialize Browser
		# Go To		http://www.google.com
		# Sleep		3s
		Go To		${Main_URL}
        Wait Until Element Is Visible       xpath://input[@value='${i}'][1]/..
        Click Element       xpath://input[@value='${i}'][1]/..
        
		Sleep 		25s
        Select Window           NEW

		# Wait Until Element Is Visible   //*[@id="userInfoOptions"]/li[3]/a
		Sleep		10s
		Go To 			https://book.goair.in/Agent/AgentReports


		Wait Until Element Is Visible			//*[@id="agentReportSubmit"]
		Click Element			//*[@id="agentReportSubmit"]

		Select From List By Value 		ddlTransaction		Deposit
		Click Element			//*[@id="datepicker"]
		Press Keys				//*[@id="datepicker"]		RETURN
		Click Element			//*[@id="datepicker1"]
		Press Keys				//*[@id="datepicker1"]		RETURN

		Click Element		//*[@id="btnCSV"]
		# Click Element		/html/body/div[1]/div[1]/div[2]/div/div[4]/button
		Sleep 		5s

		Move File		${download_path}/Deposit*.csv			~/Shohil/goAir/

       
		

		
		Sleep	2s
		Close Window
		Select Window			MAIN

		
       
    END



Download Indogo Reports

	
    FOR     ${i}        IN RANGE      7

		Log 			${i}
		Log 			@{indigo_id}[${i}]
		${Yeterday_Date} =		Get Current Date    result_format=%d/%m/%Y    increment=-1 day
		Go To 				https://6eappsbeta.goindigo.in/AgentPortal/Login.aspx
		Wait Until Element Is Visible			//*[@id="txtUName"]
		Input Text			txtUName		@{indigo_id}[${i}]
		Input Text 			txtPWD			@{indigo_password}[${i}]
		Click Element		btnSbmt
		Wait Until Element Is Visible			//*[@id="dvAgnt1"]/ul/li[4]
		Click Element			//*[@id="dvAgnt1"]/ul/li[4]

		Execute Javascript			document.querySelector("#MainContent_CALFRM").removeAttribute("readonly");
		Execute Javascript			document.querySelector("#MainContent_CALFRM").removeAttribute("onchange");
		Execute Javascript			document.querySelector("#MainContent_CALFRM").setAttribute("value", "${Yeterday_Date}"); 
		

		Execute Javascript			document.querySelector("#MainContent_CALTo").removeAttribute("readonly");
		Execute Javascript			document.querySelector("#MainContent_CALTo").removeAttribute("onchange");
		Execute Javascript			document.querySelector("#MainContent_CALTo").setAttribute("value", "${Yeterday_Date}"); 
		Sleep 		5s
		# Input Text 			//*[@id="MainContent_CALFRM"]		${Yeterday_Date}
		# Input Text 			//*[@id="MainContent_CALTo"]		${Yeterday_Date}

		Click Element			//*[@id="MainContent_btnExport"]
		Move File		${download_path}/numberformatting*.csv			~/Shohil/indigo/numberformatting_@{indigo_id}[${i}].csv
		Sleep		5s

       
    END

