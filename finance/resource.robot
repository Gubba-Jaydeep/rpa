*** Settings ***
Documentation     Finance
Library           SeleniumLibrary
Library			  Collections
Library			  ExcelLibrary
Library           ImapLibrary
#Library			  MailLib.py
Library           OperatingSystem
Library           String
Library			  BuiltIn
Library			  DateTime
Library			  Process
Library  		  DatabaseLibrary
Library			  MySmtpLib.py


*** Variables ***

${DBHost}         localhost
${DBName}         rpa
${DBPass}         password
${DBPort}         3306
${DBUser}         rpa
${DBTable}        finance

${Userdefined_date}             27-May-2019
${Userdefined_date2}             27/05/2019





*** Keywords ***



Initialize Suite Variables
	Connect To Database       pymysql      ${DBName}    ${DBUser}    ${DBPass}    ${DBHost}    ${DBPort}
	
	@{Browser1}=    Query     select name from ${DBTable} where identifier='Browser';
	@{Download_URL1}=    Query     select name from ${DBTable} where identifier='Download_URL';
  	@{Upload_URL1}=    Query     select name from ${DBTable} where identifier='Upload_URL';
  	@{gmail_id1}=    Query     select name from ${DBTable} where identifier='gmail_id';
  	@{gmail_password1}=    Query     select name from ${DBTable} where identifier='gmail_password';
  	@{gmail_URL1}=    Query     select name from ${DBTable} where identifier='gmail_url';
  	@{DELAY1}=    Query     select name from ${DBTable} where identifier='DELAY';
	@{file_path1}=    Query     select name from ${DBTable} where identifier='file_path';
	@{download_path1}=    Query     select name from ${DBTable} where identifier='download_path';
	@{upload_id_pass1}=    Query     select name from ${DBTable} where identifier='upload_id_pass';

	Set Global Variable				${Browser}					${Browser1[0][0]}
	Set Global Variable				${Download_URL}				${Download_URL1[0][0]}
	Set Global Variable				${Upload_URL}				${Upload_URL1[0][0]}
	Set Global Variable				${gmail_id}					${gmail_id1[0][0]}
	Set Global Variable				${gmail_password}			${gmail_password1[0][0]}
	Set Global Variable				${gmail_URL}				${gmail_URL1[0][0]}
	Set Global Variable				${DELAY}					${DELAY1[0][0]}	
	Set Global Variable				${file_path}				${file_path1[0][0]}	
	Set Global Variable				${download_path}			${download_path1[0][0]}	
	Set Global Variable				${upload_id_pass}			${upload_id_pass1[0][0]}	
	

	# Log				@{Browser}	
	# Log				@{Download_URL}
	# Log				@{Upload_URL}
	# Log				@{gmail_id}
	# Log				@{gmail_password}
	# Log				@{gmail_URL}
	# Log				@{DELAY}	

	${Yeterday_Date} =		Get Current Date    result_format=%d-%b-%Y    increment=-1 day
	${Yeterday_Date2} =		Get Current Date    result_format=%d/%m/%Y    increment=-1 day
	${MIS_Files}	Create List		AIR_DOM_BOOK		AIR_INT_BOOK		HOTEL_MIS_BOOK		pay_failure_mis			TRAIN_BOOK
	${BOS_Files}	Create List		India_activity_sale_bos		India_air_amend_bos		India_air_amend_rebook_bos		India_air_sale_bos		India_hotel_sale_bos		train_sale_bos		train_refund_bos
	Set Global Variable				${MIS_Files}
	Set Global Variable				${Yeterday_Date}
	Set Global Variable				${Yeterday_Date2}
	Set Global Variable				${BOS_Files}


Clear Downloads
	Remove Files			${download_path}/*.csv	

Initialize Selenium Params
	#Set Selenium Speed   ${DELAY}
	Set Selenium Timeout    1000
	Set Selenium Implicit Wait    60


Initialize Browser
# http://gubba.jaydeep@cleartrip.com:Password254@c@summary.cleartrip.com/mis_reports/27-May-2019
	Open Browser		http://${gmail_id}:${gmail_password}@${Download_URL}/${Yeterday_Date}/		Chrome
	Maximize Browser Window

Add Column
	[Arguments]			${value}	${file_name}
	${FILE_CONTENT}=   Get File    ${file_name}
	@{LINES}=    Split To Lines    ${FILE_CONTENT}
	${len}		Get Length		${LINES}
	FOR		${i}	IN RANGE		${len}-1
		Append To File	newddd.csv	@{LINES}[${i}]
		${contains}=    Run Keyword And Return Status    Should Contain    @{LINES}[${i}]    Success Transaction Records :
		Run Keyword If		${contains}		My Set	${i}
		#Run Keyword If		${contains}			Append To File	newddd.csv	,Date
		Append To File	newddd.csv	\n
		Exit For Loop If		${contains}
	END
	Append To File	newddd.csv	\n
	Append To File	newddd.csv	@{LINES}[${idx}],Date
	FOR		${i}	IN RANGE	${idx}+2	${len}-1
		Append To File	newddd.csv	@{LINES}[${i}]
		Append To File	newddd.csv	,${value}
		Append To File	newddd.csv	\n
	END

My Set
	[Arguments]		${value}
	${idx}=		Set Variable	${value}


Add Extra RTR Details
	[Arguments]		${source_file_name}
	${FILE_CONTENT}=	Get File	${source_file_name}
	Remove File			${source_file_name}
	Append To File		${source_file_name}		${FILE_CONTENT}
	${Extra_Content}=		Get File		extra.csv
	Append To File		${source_file_name}		${Extra_Content}


Remove pending Data
	[Arguments]			${source_file_name}			${idx}
	${FILE_CONTENT}=   Get File    ${source_file_name}
	Remove File			${source_file_name}
	@{LINES}=    Split To Lines    ${FILE_CONTENT}
	${len}		Get Length		${LINES}
	FOR		${i}	IN RANGE		${idx}+2
		Append To File		${source_file_name}			@{LINES}[${i}]
		Append To File		${source_file_name}			\n
	END

	FOR		${i}	IN RANGE		${idx}+2		${len}-1
		${contains}=    Run Keyword And Return Status    Should Contain    @{LINES}[${i}]    Pending
		Run Keyword Unless		${contains}		 	Append To File		${source_file_name}			@{LINES}[${i}]
		Run Keyword Unless		${contains}		 	Append To File		${source_file_name}			\n
	END



Process For RTR
	[Arguments]		${source_file_name}
	${idx}=		Set Variable		-1
	${FILE_CONTENT}=   Get File    ${source_file_name}
	@{LINES}=    Split To Lines    ${FILE_CONTENT}
	${len}		Get Length		${LINES}
	FOR		${i}	IN RANGE		${len}-1
		${contains}=    Run Keyword And Return Status    Should be equal as strings        @{LINES}[${i}]    Refund Transaction Record Details :
		Log			con:${contains}
		${idx}=		Set Variable If			${contains}				${i}			-1
		Exit For Loop If		${contains}
	END
	Log   idx:${idx}
	Run Keyword If			${idx}==-1		Add Extra RTR Details		${source_file_name}
	...			ELSE		Remove pending Data			${source_file_name}			${idx}




Make SCol Null
	[Arguments]		${hotel_file_name}
	${FILE_CONTENT}=	Get File	${hotel_file_name}
	Remove File			${hotel_file_name}
	@{LINES}=    Split To Lines    ${FILE_CONTENT}
	Append To File		${hotel_file_name}			@{LINES}[0]
	Append To File		${hotel_file_name}			\n
	Remove From List    ${LINES}    0
	${len}		Get Length		${LINES}
	FOR			${LINE}			IN			@{LINES}
		@{COLUMNS}=    Split String    ${LINE}    separator=,
		InnerLoop  ${COLUMNS}			${hotel_file_name}
		Append To File		${hotel_file_name}			\n
	END

InnerLoop
	[Arguments]		${COLUMNS}			${hotel_file_name}
	${l2}		Get Length		${COLUMNS}
	FOR		${i}		IN RANGE	${l2}-1
		${toChange}=	Run Keyword And Return Status	Should be equal as strings    ${i}    18
		Run Keyword If		${toChange}		Append To File		${hotel_file_name}			""
		...			ELSE	Append To File		${hotel_file_name}			@{COLUMNS}[${i}]
		Append To File		${hotel_file_name}			,
	END

Open Upload Page
	Go To			${Upload_URL}/Upload/UploadCTRFile/

Open Download Page
	Go To		${Download_URL}/		

Download Files
  	[Arguments]   ${date}     ${type}

  	Go To 			${Download_URL}/${date}/
	Remove Files			${download_path}/${type}_*.csv
  	Wait Until Element is Visible  //*[contains(text(), "${type}")]
  	Click Element  //*[contains(text(), "${type}")]
  	Wait Until Keyword Succeeds		200 times		20 sec			File Should Exist			${download_path}/${type}_*.csv
	Move File		${download_path}/${type}_*.csv			${file_path}/${date}/





Upload with locator
	[Arguments]			${fileName}		${type}		${checkbox_locator}
	#${checkbox_locator}=		Convert To Integer		${checkbox_locator}
	#Log			(//input[@class='chk${type}'])[${checkbox_locator}]
	Wait Until Element Is Visible		(//input[@class='chk${type}'])[${checkbox_locator}]
	Click Element			(//input[@class='chk${type}'])[${checkbox_locator}]
	Choose File			id=fileupload${type}				${fileName}
	Click Element		btnSubmit${type}
	Sleep		10s
	Wait Until Element Is Visible		(//input[@class='chk${type}'])[${checkbox_locator}]
	



Download All MIS
	[Arguments]			${date}=${EMPTY}
	${date}=		Set Variable If				"${date}"=="${EMPTY}"			 ${Yeterday_Date}			${date}
	#Open Download Page
	FOR		${i}		IN RANGE		5
		Download Files  ${date}  @{MIS_Files}[${i}]
	END

Upload All MIS
	[Arguments]			${date}=${EMPTY}
	${date}=		Set Variable If				"${date}"=="${EMPTY}"			 ${Yeterday_Date}			${date}
	Open Upload Page
	FOR		${i}		IN RANGE		5
		${req_file}=		List Files In Directory			${file_path}/${date}		@{MIS_Files}[${i}]_*.csv		absolute		
		Upload with locator			@{req_file}[0]		MIS			${i}+1	

	END
	
	#Upload with locator			${download_path}/AIR_${date1}.csv		MIS			1	#Air Domestic
	#Upload with locator			/home/gubbajaydeep/Downloads/rpa/ExcelRPA/dest.csv		MIS			2	#Air International
	#Upload with locator			/home/gubbajaydeep/Downloads/rpa/ExcelRPA/dest.csv		MIS			3	#Hotel
	#Upload with locator			/home/gubbajaydeep/Downloads/rpa/ExcelRPA/dest.csv		MIS			4	#MIS Payment Failure
	#Upload with locator			/home/gubbajaydeep/Downloads/rpa/ExcelRPA/dest.csv		MIS			5	#Train


Download All BOS
	[Arguments]			${date}=${EMPTY}
	${date}=		Set Variable If				"${date}"=="${EMPTY}"			 ${Yeterday_Date}			${date}
	
	#Open Download Page
	FOR		${i}		IN RANGE		7
		Download Files  ${date}  @{BOS_Files}[${i}]
	END
	

Upload All BOS
	[Arguments]			${date}=${EMPTY}
	${date}=		Set Variable If				"${date}"=="${EMPTY}"			 ${Yeterday_Date}			${date}
	Open Upload Page
	FOR		${i}		IN		0	3	4	5	
		${req_file}=		List Files In Directory			${file_path}/${date}		@{BOS_Files}[${i}]_*.csv		absolute		
		Upload with locator			@{req_file}[0]		BOS			${i}+1	
	END
	Go To 	   			${Upload_URL}/UploadRefundBOS/UploadRefundBOSFile
	${req_file}=		List Files In Directory			${file_path}/${date}		@{BOS_Files}[6]_*.csv		absolute		
	Upload with locator			@{req_file}[0]		BOS			4
	#Upload with locator  /home/gubbajaydeep/Downloads/rpa/ExcelRPA/dest.csv  BOS  1		#Activity Sale BOS
	#Upload with locator  /home/gubbajaydeep/Downloads/rpa/ExcelRPA/dest.csv  BOS  2		#AIR Amend BOS
	#Upload with locator  /home/gubbajaydeep/Downloads/rpa/ExcelRPA/dest.csv  BOS  3		#AIR Amend Rebook BOS
	#Upload with locator  /home/gubbajaydeep/Downloads/rpa/ExcelRPA/dest.csv  BOS  4		#AIR Sale BOS
	#Upload with locator  /home/gubbajaydeep/Downloads/rpa/ExcelRPA/dest.csv  BOS  5		#Hotel Sale BOS
	#Upload with locator  /home/gubbajaydeep/Downloads/rpa/ExcelRPA/dest.csv  BOS  6		#Train Sale BOS
	#Go To 	   			${Upload_URL}/UploadRefundBOS/UploadRefundBOSFile
	#Upload with locator  /home/gubbajaydeep/Downloads/rpa/ExcelRPA/dest.csv  BOS  4		#Train Refund Bos


Download All CT Pay ID
	[Arguments]			${date}=${EMPTY}
	${date}=		Set Variable If				"${date}"=="${EMPTY}"			 ${Yeterday_Date}			${date}
	Go to  https://mail.google.com
  	Wait Until Element is Visible  xpath://*[@id='aso_search_form_anchor']/div/input
  	Click Element  xpath://*[@id='aso_search_form_anchor']/div/input
  	Input Text  xpath://*[@id='aso_search_form_anchor']/div/input  Payment and refunds data as on
  	Press Keys  xpath://*[@id='aso_search_form_anchor']/div/input  RETURN
  	Wait Until Element is Visible  //*[@id=":1"]//div[2]/div[4]/div/div/table/tbody/tr[1]
  	Click Element  //*[@id=":1"]//div[2]/div[4]/div/div/table/tbody/tr[1]
  	Press Keys  xpath://*[@id=":1"]//div[2]/div[4]/div/div/table/tbody/tr[1]  LEFT

  	Wait Until Element is Visible  xpath:/html/body//div[3]/div/div[3]/div[2]/div[2]/div[2]/div[1]
  	Click Element  xpath:/html/body//div[3]/div/div[3]/div[2]/div[2]/div[2]/div[1]
  	Go to  https://mail.google.com
	#Open Download Page

	Sleep	10s
	${path}=		Normalize Path		${download_path}
	Run Process			 gunzip payment_data_*.csv.gz		 shell=TRUE 		cwd=${path}
	${req_file}=		List Files In Directory			${download_path}		payment_data_*.csv		absolute
			
	Move File		${download_path}/payment_data_*.csv			${file_path}/${date}/



Upload Cleartrip Pay ID
	[Arguments]			${date}=${EMPTY}
	${date}=		Set Variable If				"${date}"=="${EMPTY}"			 ${Yeterday_Date}			${date}
	Open Upload Page
	
	${req_file}=		List Files In Directory			${file_path}/${date}		payment_data_*.csv		absolute		
	Upload with locator			@{req_file}[0]		CleartripPayID			1
	#Upload with locator  /home/gubbajaydeep/Downloads/rpa/ExcelRPA/dest.csv  CleartripPayID  1		#Cleartrip Pay ID

Upload PG Data
	[Arguments]			${date}=${EMPTY}
	${date}=		Set Variable If				"${date}"=="${EMPTY}"			 ${Yeterday_Date}			${date}
	Open Upload Page
	Go To 	   			${Upload_URL}/Upload/UploadFile
	# ${j}=		Set Variable	0
	# FOR		${i}		IN			57		58		59
	# 	${req_file}=		List Files In Directory			${file_path}/${date}		@{MIS_Files}[${j}]_*.csv		absolute		
	# 	Upload with locator			@{req_file}[0]		MIS			${i}+1	
	# 	${j}=		Evaluate	${j} + 1

	# END
	${req_file}=		List Files In Directory			${file_path}/${date}		*_L2220_*.csv		absolute		
	Upload with locator			@{req_file}[0]		\			57	
	${req_file}=		List Files In Directory			${file_path}/${date}		*_L4797_*.csv		absolute		
	Upload with locator			@{req_file}[0]		\			58	
	${req_file}=		List Files In Directory			${file_path}/${date}		*_L13307_*.csv		absolute		
	Upload with locator			@{req_file}[0]		\			59	





Process Files
	[Arguments]			${date}=${EMPTY}
	${date}=		Set Variable If				"${date}"=="${EMPTY}"			 ${Yeterday_Date}			${date}


	#Make S column of Hotel_Sale_Bos as ""
	${req_file}=		List Files In Directory			${file_path}/${date}		India_hotel_sale_bos_*.csv		absolute
	Make SCol Null		@{req_file}[0]


	#For PG Data-RTR Data
	${req_file}=		List Files In Directory			${file_path}/${date}		*_L2220_*.csv		absolute		
	Process For RTR  		@{req_file}[0]
	${req_file}=		List Files In Directory			${file_path}/${date}		*_L4797_*.csv		absolute		
	Process For RTR  		@{req_file}[0]
	${req_file}=		List Files In Directory			${file_path}/${date}		*_L13307_*.csv		absolute		
	Process For RTR  		@{req_file}[0]



Move All PG Data
	[Arguments]			${date}=${EMPTY}
	${date}=		Set Variable If				"${date}"=="${EMPTY}"			 ${Yeterday_Date}			${date}

	Move File		${download_path}/*_L2220_*.csv			${file_path}/${date}/
	Move File		${download_path}/*_L4797_*.csv			${file_path}/${date}/
	Move File		${download_path}/*_L13307_*.csv			${file_path}/${date}/


Upload Login
	Go To 		${Upload_URL}/
	Input Text 		id:Username			${upload_id_pass}
	Input Text 		id:Password			${upload_id_pass}
	Click Element		id:btnlogin




#Nikhil's work below

Download All PG Data
  Go To         ${gmail_URL} 
  Enter credentials
  Search the MID L2220
  Open the latest email for MID L2220
  Download the attachment L2220
  Open Mailbox for L4797
  Open the latest email for MID L4797
  Download the attachment L4797
  Open Mailbox for L13307
  Open the latest email for MID L13307
  Download the attachment L13307






Enter credentials
  Input Text  id:identifierId  		${gmail_id}
  Click Element  xpath://*[@id="identifierNext"]
  Wait Until Element Is Visible  xpath://*[@id='password']/div[1]/div/div[1]/input
  Click Element  xpath://*[@id='password']/div[1]/div/div[1]/input
  Input Text  xpath://*[@id='password']/div[1]/div/div[1]/input  		${gmail_password}
  Wait Until Element Is Visible  xpath://*[@id="passwordNext"]
  Click Element  xpath://*[@id="passwordNext"]



Search the MID L2220
  Wait Until Element is Visible  xpath://*[@id='aso_search_form_anchor']/div/input
  Click Element  xpath://*[@id='aso_search_form_anchor']/div/input
  Input Text  xpath://*[@id='aso_search_form_anchor']/div/input  L2220
  Press Keys  xpath://*[@id='aso_search_form_anchor']/div/input  RETURN

Open the latest email for MID L2220
  Set Selenium Speed  2
  Click Element  //*[@id=":1"]//div[2]/div[4]/div/div/table/tbody/tr[1]


Download the attachment L2220
  Set Selenium Speed  2
  Wait Until Element is Visible  //*[@id=":1"]//div[2]/div[2]//div[4]/span[1]
  Click Element  //*[@id=":1"]//div[2]/div[2]//div[4]/span[1]
  Wait Until Element is Visible  xpath:/html/body//div[3]/div/div[3]/div[2]/div[2]/div[2]/div[1]
  Click Element  xpath:/html/body//div[3]/div/div[3]/div[2]/div[2]/div[2]/div[1]
  Go to  https://mail.google.com
  #Press Keys  xpath:/html/body//div[3]/div/div[3]/div[2]/div[2]/div[2]/div[1]  ESC


Open Mailbox for L4797
  Wait Until Element is Visible  xpath://*[@id='aso_search_form_anchor']/div/input
  Click Element  xpath://*[@id='aso_search_form_anchor']/div/input
  Input Text  xpath://*[@id='aso_search_form_anchor']/div/input  L4797
  Press Keys  xpath://*[@id='aso_search_form_anchor']/div/input  RETURN


Open the latest email for MID L4797
  Set Selenium Speed  2
  Click Element  //*[@id=":1"]//div[2]/div[4]/div/div/table/tbody/tr[1]

Download the attachment L4797
  Set Selenium Speed  2
  Wait Until Element is Visible  //*[@id=":1"]//div[2]/div[2]//div[4]/span[1]
  Click Element  //*[@id=":1"]//div[2]/div[2]//div[4]/span[1]
  Wait Until Element is Visible  xpath:/html/body//div[3]/div/div[3]/div[2]/div[2]/div[2]/div[1]
  Click Element  xpath:/html/body//div[3]/div/div[3]/div[2]/div[2]/div[2]/div[1]
  Go to  https://mail.google.com


Open Mailbox for L13307
  #Click Element  xpath://html/body/div[7]/div[3]//div[2]/div[1]/div[1]/div[1]/div/div/div[1]/div/div/div/div[1]/div/div[1]/div/div[1]
  Wait Until Element is Visible  xpath://*[@id='aso_search_form_anchor']/div/input
  Click Element  xpath://*[@id='aso_search_form_anchor']/div/input
  Input Text  xpath://*[@id='aso_search_form_anchor']/div/input  L13307
  Press Keys  xpath://*[@id='aso_search_form_anchor']/div/input  RETURN


Open the latest email for MID L13307
  Set Selenium Speed  2
  Click Element  //*[@id=":1"]//div[2]/div[4]/div/div/table/tbody/tr[1]

Download the attachment L13307
  Set Selenium Speed  2
  Wait Until Element is Visible  //*[@id=":1"]//div[2]/div[2]//div[4]/span[1]
  Click Element  //*[@id=":1"]//div[2]/div[2]//div[4]/span[1]
  Wait Until Element is Visible  xpath:/html/body//div[3]/div/div[3]/div[2]/div[2]/div[2]/div[1]
  Click Element  xpath:/html/body//div[3]/div/div[3]/div[2]/div[2]/div[2]/div[1]


Generate Reconsilation
	[Arguments]			${date2}=${EMPTY}
	${date2}=		Set Variable If				"${date2}"=="${EMPTY}"			 ${Yeterday_Date2}			${date2}

	Remove Files			${download_path}/*.xlsx	
	Go To		http://192.168.104.39/Cleartrip_PGPartII/Reports/GenerateRecon
	Click Element		(//input[@id='chhk'])[57]
	Click Element		(//input[@id='chhk'])[58]
	Click Element		(//input[@id='chhk'])[59]
	Input Text		txtdate			${date2}
	Sleep		5s
	Click Element		btnGenerate
	Sleep 		20s


Process Recon
	[Arguments]			${date}=${EMPTY}
	${date}=		Set Variable If				"${date}"=="${EMPTY}"			 ${Yeterday_Date}			${date}
	
	Move File		${download_path}/Recon_*.xlsx			${file_path}/${date}/
	${recon_filename}=		List Files In Directory			${file_path}/${date}		Recon_*.xlsx		absolute		
	Open Excel Document		@{recon_filename}[0]  	doc1
	
	
	# ${path}=		Normalize Path		${file_path}/${date}
	# Run Process			 ssconvert pay_failure_mis_*.csv pay_failure_mis.xlsx		 shell=TRUE 		cwd=${path}
	# Sleep 	10s
	# ${path}=		Normalize Path		${file_path}/${date}/pay_failure_mis.xlsx
	# Open Excel Document			${path}		  	doc2

	#process starts here
	Go To		https://${gmail_id}:${gmail_password}@www.cleartrip.com/hq/trips/
	Input Text		email		nikhil.gupta@cleartrip.com
	Input Text 		password		hellomother@12
	Press Keys  	//*[@id="password"]		RETURN
	Sleep 		10s

	${idx}=		Set Variable	5			
	FOR		${i}		IN RANGE		1   500
		Switch Current Excel Document		doc1
		${r}=		Read Excel Cell			row_num=${i}		col_num=2			sheet_name=Report-3
		Continue For Loop If		 "${r}"=="${None}"
		Continue For Loop If		 "${r}"=="0"
		# ${r}=		Convert To Integer		${r}
		Continue For Loop If			len("${r}")<12

		Go To		https://www.cleartrip.com/hq/trips/${r}
		Sleep		5s

		# ${temp1}=		Run Keyword and Return Status		Page Should Contain				Booking Initialized
		# ${temp2}=		Run Keyword and Return Status		Page Should Contain				Payment Failed
		${temp1}=		Run Keyword and Return Status		Element Should Contain		//*[@id="left"]/div[2]				Booking Initialized
		${temp2}=		Run Keyword and Return Status		Element Should Contain		//*[@id="left"]/div[2]				Payment Failed

		Log		${temp1}
		Log		${temp2}
		Run Keyword If		${temp1} or ${temp2}			Copy it into Report4		${idx}		${i}
		${t3}=			Evaluate		${idx} + 1
		${idx}=		Set Variable If		${temp1} or ${temp2}			${t3}		${idx}

		Save Excel Document		@{recon_filename}[0]

	END
	Close All Excel Documents

Copy it into Report4
	[Arguments]			${idx}		${i}
	Switch Current Excel Document		doc1
	${c1}=		Read Excel Cell			row_num=${i}		col_num=1			sheet_name=Report-3
	${c2}=		Read Excel Cell			row_num=${i}		col_num=2			sheet_name=Report-3
	${c3}=		Read Excel Cell			row_num=${i}		col_num=3			sheet_name=Report-3
	${c4}=		Read Excel Cell			row_num=${i}		col_num=4			sheet_name=Report-3
	${c5}=		Read Excel Cell			row_num=${i}		col_num=5			sheet_name=Report-3
	${c6}=		Read Excel Cell			row_num=${i}		col_num=6			sheet_name=Report-3
	${c7}=		Read Excel Cell			row_num=${i}		col_num=7			sheet_name=Report-3

	Write Excel Cell		row_num=${idx}		col_num=1		value=${c1}			sheet_name=Report-4
	Write Excel Cell		row_num=${idx}		col_num=2		value=${c2}			sheet_name=Report-4
	Write Excel Cell		row_num=${idx}		col_num=3		value=${c3}			sheet_name=Report-4
	Write Excel Cell		row_num=${idx}		col_num=4		value=${c4}			sheet_name=Report-4
	Write Excel Cell		row_num=${idx}		col_num=5		value=${c5}			sheet_name=Report-4
	Write Excel Cell		row_num=${idx}		col_num=6		value=${c6}			sheet_name=Report-4
	Write Excel Cell		row_num=${idx}		col_num=7		value=${c7}			sheet_name=Report-4




Send Recon Mail usinf Python Library
	[Arguments]			${date}=${EMPTY}
	${date}=		Set Variable If				"${date}"=="${EMPTY}"			 ${Yeterday_Date}			${date}

	${recon_filename}=		List Files In Directory			${file_path}/27-May-2019		Recon_*.xlsx		absolute
    Send Mail           rpa.finance@cleartrip.com       Rcleartrip@12       jaydeepgubba@gmail.com          @{recon_filename}[0]

Send Recon Mail
	[Arguments]			${date}=${EMPTY}
	${date}=		Set Variable If				"${date}"=="${EMPTY}"			 ${Yeterday_Date}			${date}

	${recon_filename}=		List Files In Directory			${file_path}/27-May-2019		Recon_*.xlsx		absolute
	${path}=		Normalize Path		${file_path}/${date}
	Run Process			  echo "message body" | mail -s "this is subject" jaydeepgubba@gmail.com -A ${recon_filename}[0]		 shell=TRUE 		cwd=${path}
	
