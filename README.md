# Robotic Process Automation

One Paragraph of project description goes here

## Getting Started

Run the below commands.
These commands are only for unix based systems.




### Update 

```
sudo apt update
```

### Install Selenium Chrome Driver

```
wget https://chromedriver.storage.googleapis.com/74.0.3729.6/chromedriver_linux64.zip
unzip chromedriver_linux64.zip
sudo mv chromedriver /usr/bin/chromedriver
sudo chown root:root /usr/bin/chromedriver
sudo chmod +x /usr/bin/chromedriver
```

### Install Python 3.x
```
sudo apt install python3-pip
```

### Install Robot Framework
```
sudo -H pip3 install robotframework
```

### Install All the required Robot framework Libraries
```

pip3 install robotframework-imaplibrary
pip3 install robotframework-databaselibrary
pip3 install robotframework-debuglibrary
pip3 install robotframework-selenium2library
pip3 install robotframework-seleniumlibrary
pip3 install robotframework-excellib
pip3 install robotframework-csvlib

```

### Install MySQL
```
sudo apt install mysql-server
sudo mysql_secure_installation
```

### Install pymysql python3 library
```
sudo apt-get install python3-pymysql
```

### To Setup MySQL
```
sudo apt update
sudo apt install mysql-server
sudo mysql_secure_installation
to check users>>  SELECT user,authentication_string,plugin,host FROM mysql.user;
to change root password >> ALTER USER 'root'@'localhost' IDENTIFIED WITH mysql_native_password BY 'password';
FLUSH PRIVILEGES;

CREATE USER 'rpa'@'localhost' IDENTIFIED BY 'password';
GRANT ALL PRIVILEGES ON *.* TO 'rpa'@'localhost' WITH GRANT OPTION;

to check if mysql working >> systemctl status mysql.service


CREATE DATABASE rpa;
```

### Run these in MySQL Shell
```
use rpa;
CREATE TABLE finance (identifier varchar(200) unique,name varchar(200));
insert into finance values('Browser','Chrome');
insert into finance values('Download_URL','summary.cleartrip.com/mis_reports');
insert into finance values('Upload_URL','http://192.168.104.39/Cleartrip_PGPartII');
insert into finance values('gmail_id','rpa.finance@cleartrip.com');
insert into finance values('gmail_password','Rcleartrip@12');
insert into finance values('gmail_url','https://mail.google.com');
insert into finance values('DELAY','0.3');
insert into finance values('file_path','~/Finance_Files');
insert into finance values('download_path','~/Downloads');
insert into finance values('upload_id_pass','mandar');
commit;
```


## Author

* **Gubba Jaydeep** - *Initial work* - [Socio Generali](https://github.com/Gubba-Jaydeep/SocioGenerali)

See also the list of [contributors](https://github.com/your/project/contributors) who participated in this project.

## License

This project is licensed under Cleartrip pvt ltd



